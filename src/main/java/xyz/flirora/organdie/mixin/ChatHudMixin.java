package xyz.flirora.organdie.mixin;

import net.minecraft.client.gui.hud.ChatHud;
import net.minecraft.client.gui.hud.MessageIndicator;
import net.minecraft.text.Text;
import net.minecraft.util.Nullables;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ChatHud.class)
public class ChatHudMixin {
	@Shadow @Final private static Logger LOGGER;

	@Inject(at = @At("HEAD"), method = "logChatMessage", cancellable = true)
	private void myLogChatMessage(Text message, @Nullable MessageIndicator indicator, CallbackInfo info) {
		String stringifiedMessage = Text.Serialization.toJsonString(message);
		String stringifiedIndicator = Nullables.map(indicator, MessageIndicator::loggedName);
		if (stringifiedIndicator != null) {
			LOGGER.info("[{}] [CHAT] {}", stringifiedIndicator, stringifiedMessage);
		} else {
			LOGGER.info("[CHAT] {}", stringifiedMessage);
		}
		info.cancel();
	}
}